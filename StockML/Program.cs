﻿using Microsoft.ML;
using Microsoft.ML.Runtime.Api;
using Microsoft.ML.Runtime.Data;
using Microsoft.ML.Runtime.Learners;
using Microsoft.ML.Transforms.Conversions;
using System;


namespace StockML
{
    public class Program
    {
        private const int PREDICTION_YEARS = 8;

        static void Main(string[] args)
        {
            Console.WriteLine("Stock Price Predictor");

            // quandl key = PoEDSJN1H6XSP8sppyCb
            // https://www.quandl.com/api/v3/datasets/WIKI/FB/data.csv?api_key=PoEDSJN1H6XSP8sppyCb
            // https://www.quandl.com/api/v3/datasets/WIKI/FB/data.csv
            // https://www.quandl.com/api/v3/datasets/WIKI/FB/data.csv?column_index=4&exclude_column_names=true&rows=3&start_date=2012-11-01&end_date=2013-11-30&order=asc&collapse=quarterly&transform=rdiff

            var mlContext = new MLContext();

            string dataPath = "WIKI-FB.csv";

            var reader = mlContext.Data.TextReader(new TextLoader.Arguments()
            {
                Separator = ",",
                HasHeader = true,
                Column = new[]
                {
                    //Date,Open,High,Low,Close,Volume,Ex-Dividend,Split Ratio,Adj. Open,Adj. High,Adj. Low,Adj. Close,Adj. Volume
                    new TextLoader.Column("Date", DataKind.Text, 0),
                    new TextLoader.Column("Open", DataKind.R4, 1),
                    new TextLoader.Column("High", DataKind.R4, 2),
                    new TextLoader.Column("Low", DataKind.R4, 3),
                    new TextLoader.Column("Close", DataKind.R4, 4),
                    new TextLoader.Column("Volume", DataKind.R4, 5),
                    new TextLoader.Column("ExDividend", DataKind.R4, 6),
                    new TextLoader.Column("Split", DataKind.R4, 7),
                    new TextLoader.Column("Ratio", DataKind.R4, 8),
                    new TextLoader.Column("AdjOpen", DataKind.R4, 9),
                    new TextLoader.Column("AdjHigh", DataKind.R4, 10),
                    new TextLoader.Column("AdjClose", DataKind.R4, 11),
                    new TextLoader.Column("AdjVolume", DataKind.R4, 12)
                }
            });

            IDataView trainingDataView = reader.Read(new MultiFileSource(dataPath));

            // STEP 3: Transform your data and add a learner
            // Assign numeric values to text in the "Label" column, because only numbers can be processed during model training.
            // Add a learning algorithm to the pipeline. 
            // Convert the Label back into original text (after converting to number in step 3)
            var pipeline = mlContext.Transforms.Conversion.MapValueToKey("Date")
                .Append(mlContext.Transforms.Concatenate("Features", "Open", "High", "Low", "Close", "Volume", "ExDividend", "Split", "Ratio", "AdjOpen", "AdjHigh", "AdjClose", "AdjVolume"))
                .Append(mlContext.MulticlassClassification.Trainers.StochasticDualCoordinateAscent(labelColumn: "Date", featureColumn: "Features"))
                .Append(mlContext.Transforms.Conversion.MapKeyToValue("PredictedLabel"));



            // STEP 4: Train your model based on the data set  
            var model = pipeline.Fit(trainingDataView);

            // STEP 5: Use your model to make a prediction
            // You can change these numbers to test different predictions
            var prediction = model.MakePredictionFunction<StockData, PricePrediction>(mlContext).Predict(
                new StockData()
                {
                    Open = 150.0f,
                    High = 25.0f,
                });

            Console.WriteLine($"Predicted price is: {prediction.PredictedLabels}");

            Console.Read();
        }
    }
}
