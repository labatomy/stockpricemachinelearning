﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.ComponentModel.DataAnnotations;

// Test 1
namespace StockML
{
    public class StockData
    {
        // Date, Open, High, Low, Close, Volume, Ex-Dividend, Split, Ratio, Adj. Open, Adj. High, Adj. Low, Adj. Close, Adj. Volume

        [Column("Date")]
        public string Date;

        [Column("Open")]
        public float Open;

        [Column("High")]
        public float High;

        [Column("Low")]
        public float Low;

        [Column("Close")]
        public float Close;

        [Column("Volume")]
        public float Volume;

        [Column("ExDividend")]
        public float ExDividend;

        [Column("Split")]
        public float Split;

        [Column("Ratio")]
        public float Ratio;

        [Column("AdjOpen")]
        public float AdjOpen;

        [Column("AdjHigh")]
        public float AdjHigh;

        [Column("AdjClose")]
        public float AdjClose;

        [Column("AdjVolume")]
        public float AdjVolume;

    }
}
